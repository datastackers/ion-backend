import { Bootstrap, ZuuOptions } from "@zuu/bootstrap";
import { Debugger, Runtime } from "@zuu/vet";
import { Timer } from './packages/timer/Timer';
import { AliveController } from './controllers/AliveController';
import { DefaultController } from './controllers/DefaultController';
import { ListeningEventListener } from './listeners/ListeningEventListener';
import { Card } from './model/entities/Card';
import { User } from './model/entities/User';
import { Not } from "@zuu/ferret";
import { CardResolver } from './resolvers/CardResolver';

Debugger.deafults();

let tag = Debugger.tag("service-default");

let options: ZuuOptions = {
    server: {
        port: parseInt(process.env["PORT"]) || 4100,
        modules: []
    },
    listeners: [
        new ListeningEventListener
    ],
    controllers: [
        AliveController,
        DefaultController
    ],
    cors: true
};

let timer = new Timer().reset();
Runtime.scoped(null, async _ => {
    Debugger.log(tag`Initialization began!`);
    let { app } = await Bootstrap.scope(options).run();
    return (typeof app != "undefined" && app != null);
})
.then(async result => {
    Debugger.log(tag`Initialization succeeded! Took ${timer.stop().diff()}ms!`);
    // let card = new Card();
    // card.identity = "BC:EF:CB:FE";
    // await card.save();
    let resolver = new CardResolver();

    // let user = await User.findOne({ where: { email: "test10@dev.com" } })
    // console.log(user);
    // let account = (await user.accounts)[0];
    // account.value = 300;
    // await account.save();
    // let cards = await Card.find();
    // for(let card of cards) {
    //     await resolver.allocate(card, account);
    // }
    // await resolver.allocate(card, account);
    // let card = await Card.findOne({where: {identity: "AB:BA:AB:BA"}});
    // card.identity = "DF:88:C5:A4";
    // await card.save();
})
.catch(Debugger.error);