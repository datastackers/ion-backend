import { AuthedGraphQLController, Bootstrap, ZuuOptions } from "@zuu/bootstrap";
import { Debugger, Runtime } from "@zuu/vet";
import { AuthController } from './controllers/AuthController';
import { ResponseFormatter } from './interceptors/ResponseFormatter';
import { ListeningEventListener } from './listeners/ListeningEventListener';
import { ResponseTime } from "./middlewares/ResponseTime";
import { Timer } from './packages/timer/Timer';
import { MeResolver } from './resolvers/MeResolver';
import { currentUserChecker, contextFiller, subscriptionCurrentUserChecker } from "./packages/auth/Checks";
import { AliveController } from './controllers/AliveController';

Debugger.deafults();

let tag = Debugger.tag("service-users");

let options: ZuuOptions = {
    server: {
        port: parseInt(process.env["PORT"]) || 4100,
        modules: []
    },
    currentUserChecker,
    graph: {
        contextFiller, subscriptionCurrentUserChecker
    },
    listeners: [
        new ListeningEventListener
    ],
    middlewares: [
        ResponseTime
    ],
    interceptors: [
        ResponseFormatter
    ],
    controllers: [
        AliveController,
        AuthController,
        AuthedGraphQLController
    ],
    resolvers: [
        MeResolver
    ],
    cors: true
};

let timer = new Timer().reset();
Runtime.scoped(null, async _ => {
    Debugger.log(tag`Initialization began!`);
    let { app } = await Bootstrap.scope(options).run();
    return (typeof app != "undefined" && app != null);
})
.then(async result => {
    Debugger.log(tag`Initialization succeeded! Took ${timer.stop().diff()}ms!`);
})
.catch(Debugger.error);