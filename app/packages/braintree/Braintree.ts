import { BraintreeConfig } from '../../configs/Braintree';
import { Configuration } from '@zuu/vet';

import * as braintree from "braintree";

export class Braintree {
    @Configuration("braintree")
    private static config: BraintreeConfig;

    private gateway; 
    public constructor() {
        this.gateway = braintree.connect({
            environment: braintree.Environment.Sandbox,
            merchantId: Braintree.config.id,
            publicKey: Braintree.config.public,
            privateKey: Braintree.config.private
        });
    }

    public async createCustomer(data: { email: string, firstName: string, lastName: string }): Promise<string> {
        return await (new Promise<string>((resolve, reject) => {
            this.gateway.customer.create(data, function (err, result) {
                if(err) return reject(err);
                resolve(result.customer.id);
            });
        }));
    }

    public async clientToken(id?: string): Promise<string> {
        let options = { customerId: id };
        if(!id) delete options.customerId;

        return await (new Promise<string>((resolve, reject) => {
            this.gateway.clientToken.generate(options, function (err, response) {
                if(!!err) return reject(err);
                console.log(response);
                resolve(response.clientToken);
            });
        }));
    }

    public async sale(ammount: string, currency: string, nonce: string): Promise<boolean> {
        return await (new Promise<boolean>((resolve, reject) => {
            this.gateway.transaction.sale({
                amount: ammount,
                // currency: currency.toUpperCase(),
                paymentMethodNonce: nonce,
                options: {
                    submitForSettlement: true
                }
            }, function (err, result) {
                if(!!err) return reject(err);
                resolve(true);
            });
        }));
    }
}