import { User } from "../../model/entities/User";
import { Account } from '../../model/entities/Account';
import { Topup } from '../../model/entities/Topup';
import { Merchant } from '../../model/entities/Merchant';
import { Payment } from '../../model/entities/Payment';
import { Card } from '../../model/entities/Card';

export interface Context { 
    user?: User;
    account?: Account;
    topup?: Topup;
    merchant?: Merchant;
    payment?: Payment;
    card?: Card;
};