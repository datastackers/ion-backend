import { UnresolvableResourceAccessError } from '../../errors/UnresolvableResourceAccessError';
import { UnauthorizedResourceAccessAttemptError } from "../../errors/UnauthorizedResourceAccessAttemptError";
import { User } from "../../model/entities/User";
import { Context } from "./Context";
import { Account } from '../../model/entities/Account';
import { Topup } from '../../model/entities/Topup';
import { Payment } from '../../model/entities/Payment';
import { Merchant } from '../../model/entities/Merchant';
import { Card } from '../../model/entities/Card';

export class HeadersContextFiller {
    public static async fill(user: User, headers: any): Promise<Object> {
        let context: Context = { };
        
        let accountId = headers["x-resource-account"];
        let topupId = headers["x-resource-topup"];
        let paymentId = headers["x-resource-payment"];
        let merchantId = headers["x-resource-merchant"];
        let cardId = headers["x-resource-card"];

        if(typeof accountId != "undefined") {
            let account = await Account.findOne(accountId);
            if(!account) throw new UnresolvableResourceAccessError("account", accountId);
            // if((await account.owner).id != user.id) throw new UnauthorizedResourceAccessAttemptError("account", accountId);
            
            context.account = account;
        }

        if(typeof topupId != "undefined") {
            let topup = await Topup.findOne(topupId);
            if(!topup) throw new UnresolvableResourceAccessError("topup", topupId);
            // if((await topup.user).id != user.id) throw new UnauthorizedResourceAccessAttemptError("topup", topupId);

            context.topup = topup;
        }

        if(typeof paymentId != "undefined") {
            let payment = await Payment.findOne(paymentId);
            if(!payment) throw new UnresolvableResourceAccessError("payment", paymentId);

            context.payment = payment;
        }

        if(typeof merchantId != "undefined") {
            let merchant = await Merchant.findOne(merchantId);
            if(!merchant) throw new UnresolvableResourceAccessError("merchant", merchantId);

            context.merchant = merchant;
        }

        if(typeof cardId != "undefined") {
            let card = await Card.findOne(cardId);
            if(!card) throw new UnresolvableResourceAccessError("card", cardId);

            context.card = card;
        }
        
        return context;
    }
}