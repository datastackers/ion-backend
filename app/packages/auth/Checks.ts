import { HeadersContextFiller } from '../context/HeadersContextFiller';
import { User } from '../../model/entities/User';
import { UnauthorizedError, Action } from '@zuu/mink';
import { AuthContextChecker } from '../context/AuthContextChecker';
import { ConnectionContext } from 'subscriptions-transport-ws';

export async function contextFiller(user: User, headers: any): Promise<any> {
    return await HeadersContextFiller.fill(user, headers);
};

export async function subscriptionCurrentUserChecker(connectionParams: Object, webSocket: WebSocket, context: ConnectionContext): Promise<any> {
    let token = connectionParams['x-access-token'];
    let user = await AuthContextChecker.check(token);
    if (!user) throw new UnauthorizedError();

    return { user, ...(await HeadersContextFiller.fill(user, connectionParams)) };        
};

export async function currentUserChecker(action: Action): Promise<User> {
    let token = action.request.headers["x-access-token"];
    return await AuthContextChecker.check(token);
};