export class UnknownServiceError extends Error {
    constructor(service: string) {
        super("Unknown service <" + service + ">!");
    }
}