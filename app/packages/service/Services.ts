import { Configuration } from "@zuu/vet";
import { ServiceConfig } from '../../configs/Service';
import { Network } from '../network/Network';

export enum ServiceVersion {
    NOW = "-"
};

export class Services {
    public static DEFAULT: string = "default";
    public static USERS: string = "users";

    @Configuration("service")
    private static config: ServiceConfig;

    private static cache: {[key: string]: Network} = {};

    public static urlFor(service: string, version?: string): string {
        let url: string[] = [this.config.app];
        
        if(service != this.DEFAULT) {
            let descriptor = this.config.services[service];
            if(typeof descriptor == 'string') {
                url.unshift(descriptor);        
            } else {
                url.unshift(descriptor.id);
                if(descriptor.version && descriptor.version != ServiceVersion.NOW && !version) {
                    url.unshift(descriptor.version);
                }
            }
        }

        if(version && version != ServiceVersion.NOW) {
            url.unshift(version);
        }

        return "https://" + url.join("-dot-") + ".appspot.com/";
    }

    public static sockets() {
        return "wss://" + this.config.sockets + "." + this.config.app + ".appspot.com/";
    }

    public static build(service: string, options?: { version?: string, force?: boolean }): Network {
        if(!this.cache[service] || (!!options && options.force)) {
            let url = this.urlFor(service, (options || {}).version);
            let network = new Network(url, this.sockets());
            this.cache[service] = network;
        }
        
        return this.cache[service];
    }
}