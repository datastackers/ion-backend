import { ContextProvider } from './context/ContextProvider';
import { HttpLayer } from './layers/HttpLayer';
import { GraphLayer } from './layers/GraphLayer';
import { FetchResult, Observable } from 'apollo-link';
import { Executor } from './execution/Executor';
import { Context } from './context/Context';
import * as _ from "lodash";

export class Network {
  public contextProvider: ContextProvider;
  public http: HttpLayer;
  public graph: GraphLayer;

  public constructor(url: string, socketsUrl: string) {
    this.contextProvider = new ContextProvider();
    
    this.http = new HttpLayer(url);
    this.graph = new GraphLayer(url, socketsUrl, 'api/graph', 'api/graph/subscriptions');
    this.graph.init(this.contextProvider.exportOptimalContext());
  };

  public updateContext() {
    this.graph.swapContext(this.contextProvider.exportOptimalContext());
  };

  public isAuthorized(): boolean {
    return this.contextProvider.exportOptimalContext().isAuthorized();
  };

  private copy(obj: any): any {
    return _.cloneDeep(obj);
  };

  public async scope<T = any>(executorFunction: (executor: Executor) => Promise<FetchResult<T>>, contextFunction: (context: Context) => Context | Promise<Context> = (x) => x): Promise<FetchResult<T>> {
    let ctx = this.contextProvider.exportOptimalContext();
    ctx = await contextFunction(ctx);

    let executor = new Executor(ctx, this.http, this.graph);
    let result;
    result = await executorFunction(executor);
    executor = null;
    
    return this.copy(result);
  };

  public async subscription<T = any>(executorFunction: (executor: Executor) => Promise<Observable<T>>, contextFunction: (context: Context) => Context | Promise<Context> = (x) => x): Promise<Observable<T>> {
    let ctx = this.contextProvider.exportOptimalContext();
    ctx = await contextFunction(ctx);

    let executor = new Executor(ctx, this.http, this.graph);
    let result;
    result = await executorFunction(executor);
    executor = null;
    
    return this.copy(result);
  };
};
