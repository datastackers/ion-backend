import { Token } from '../../../model/entities/Token';
/**
 * Class used as a context container
 */
export class Context {
    private static ACCESS_TOKEN_KEY: string = 'x-access-token';
    private static RESOURCE_KEY_PREFIX: string = 'x-resource-';

    private token: string;
    private headers: {[key:string]: string} = { };

    public constructor(token: string = null) {
        this.authorize(token);
    }

    /**
     * Compile and export the generated stores into a headers collection
     * @returns The compiled headers collection
     */
    public export(): {[key: string]: string} {
        return this.headers;
    }

    /**
     * Sets a value inside the headers store for later usage.
     * @param key The key in the headers store.
     * @param value The value that will be set.
     * @returns The context (for chaining)
     */
    public header(key: string, value: string): Context {
        if(!value) delete this.headers[key];
        else this.headers[key] = value;

        return this;
    }

    /**
     * Authorize the current context agains a set of credentials.
     * @param credentials Set of credentials used in the authorization process
     * @returns The context (for chaining)
     */
    public authorize(token: string): Context {
        if(!token) return this;

        if(!!token) this.token = token;

        this.header(Context.ACCESS_TOKEN_KEY, this.token);
    
        return this;
    }

    /**
     * Checks is the context is authorized.
     * 
     * @returns Authorization state
     */
    public isAuthorized(): boolean {
        return !!this.token;
    }

    /**
     * Sets a value inside the resource store for later usage.
     * @param name The name of the resource
     * @param value The value of the resource
     * @returns The context (for chaining)
     */
    public resource(name: string, value: string): Context {
        if(!!name) this.header(`${Context.RESOURCE_KEY_PREFIX}${name}`, value);

        return this;
    }

}