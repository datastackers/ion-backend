import { Context } from './Context';

/**
 * Provides the context.
 */
export class ContextProvider {
    constructor() { };

    /**
     * This method tries to export an optimal context using the 'Optimistic Provider' pattern.
     * @returns A context
     */
    public exportOptimalContext(): Context {
        let context = new Context;
        
        // let optimisticCredentials = this.credentialsProvider.export(true);
        // if(!!optimisticCredentials) {
        //     context.authorize(optimisticCredentials);
        // }

        return context;
    }
}