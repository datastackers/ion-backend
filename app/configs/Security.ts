import { IConfig } from "@zuu/vet";

export interface SecurityConfig extends IConfig {
    hash: {
        salt: {
            length: number,
            secret: string
        }
    },
    token: {
        expires: string,
        secret: string
    }
}