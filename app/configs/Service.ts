import { IConfig } from "@zuu/vet";

export interface ServiceConfig extends IConfig {
    app: string,
    sockets: string,
    services: {[key: string]: ({ id: string, version?: string } | string)}   
}