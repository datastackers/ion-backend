import { IConfig } from "@zuu/vet";

export interface BraintreeConfig extends IConfig {
    id: string;
    public: string;
    private: string;
}