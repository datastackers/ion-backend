import { Resolver, Ctx, Arg, Mutation, Float } from '@zuu/owl';
import { Topup } from '../model/entities/Topup';
import { User } from '../model/entities/User';
import { Braintree } from '../packages/braintree/Braintree';
import { Inject } from "@zuu/vet";
import { Account } from '../model/entities/Account';
import { RequiredResourceNotProvidedError } from '../errors/RequiredResourceNotProvidedError';
import { Transaction } from '../model/entities/Transaction';

@Resolver()
export class TopupResolver {
    @Inject private braintree: Braintree;

    @Mutation(returns => Topup, { nullable: false })
    public async beginTopup(
        @Ctx('user') user: User,
        @Ctx('account') account: Account,
        @Arg('value', type => Float, { nullable: false }) value: number,
        @Arg('currency', { nullable: true }) currency?: string
    ): Promise<Topup> {
        if(!account) throw new RequiredResourceNotProvidedError('account');
        
        let token = await this.braintree.clientToken(user.customer);
        let topup = new Topup(token, value, currency).init(account);    

        return topup;
    }

    @Mutation(returns => Topup, { nullable: false })
    public async finishTopup(
        @Ctx('topup') topup: Topup,
        @Arg('nonce', { nullable: false }) nonce: string
    ): Promise<Topup> {
        if(!topup) throw new RequiredResourceNotProvidedError('topup');
        let result = await this.braintree.sale(topup.value + "", topup.currency, nonce);
        if(result) {
            await new Transaction(1, topup.value).init(await topup.account);

            (await topup.account).value += topup.value;
            await (await topup.account).save();
        }
        return topup;
    }
}