import { Resolver, Query, Ctx, Arg, Mutation } from "@zuu/owl";
import { User } from '../model/entities/User';
import { Inject } from "@zuu/vet";
import { Braintree } from '../packages/braintree/Braintree';
import { Account } from '../model/entities/Account';
import { Currency } from '../packages/account/Currency';

@Resolver()
export class MeResolver {
    @Inject braintree: Braintree;

    @Query(returns => User, { nullable: false })
    public async me(
        @Ctx("user") user: User
    ): Promise<User> {
        return user;
    }

    @Mutation(returns => User, { nullable: false })
    public async update(
        @Ctx("user") user: User,
        @Arg("fullName", { nullable: false }) fullName: string
    ): Promise<User> {
        user.fullName = fullName;
        user.customer = await this.braintree.createCustomer({ email: user.email, firstName: user.firstName(), lastName: user.lastName() });
        await user.save();

        await (new Account(user.firstName() + "'s EUR Account", Currency.EUR).init(user));

        await user.save();
        return user;
    }
}