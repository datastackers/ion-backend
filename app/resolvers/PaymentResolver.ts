import { Resolver, Mutation, Ctx, Query, Arg } from '@zuu/owl';
import { Payment } from "../model/entities/Payment";
import { Merchant } from '../model/entities/Merchant';
import { RequiredResourceNotProvidedError } from "../errors/RequiredResourceNotProvidedError";
import { Account } from '../model/entities/Account';
import { Transaction } from '../model/entities/Transaction';
import { Card } from '../model/entities/Card';

@Resolver()
export class PaymentResolver {
    @Mutation(returns => Payment, { nullable: false })
    public async create(
        @Ctx("merchant") merchant: Merchant,
        @Arg("value", { nullable: false }) value: number
    ): Promise<Payment> {
        if(!merchant) throw new RequiredResourceNotProvidedError("merchant");
        return await (new Payment(value).init(merchant));
    }

    @Mutation(returns => Payment, { nullable: true })
    public async pay(
        @Ctx("payment") payment: Payment,
        @Ctx("account") account: Account
    ): Promise<Payment> {
        if(!payment) throw new RequiredResourceNotProvidedError("payment");
        if(!account) throw new RequiredResourceNotProvidedError("account");

        if(account.value < payment.value) return null;

        await (new Transaction(-1, payment.value).init(account));

        account.value -= payment.value;
        await account.save();

        await payment.finish(account);
        
        return payment;
    }

    @Query(type => Payment)
    public async payment(
        @Ctx("payment") payment: Payment
    ): Promise<Payment> {
        if(!payment) throw new RequiredResourceNotProvidedError("payment");
        return payment;
    }
}