import { Resolver, Ctx, Mutation, Arg, Query } from '@zuu/owl';
import { Card } from "../model/entities/Card";
import { User } from '../model/entities/User';
import { RequiredResourceNotProvidedError } from '../errors/RequiredResourceNotProvidedError';
import { Account } from '../model/entities/Account';

@Resolver()
export class CardResolver {
    @Mutation(returns => Card, { nullable: false })
    public async pair(
        @Ctx('user') user: User,
        @Ctx('card') card: Card
    ): Promise<Card> {
        if(!card) throw new RequiredResourceNotProvidedError("card");
        
        await card.pair(user);        
        return card;
    }

    @Mutation(returns => Card, { nullable: true })
    public async allocate(
        @Ctx('card') card: Card,
        @Ctx('account') account: Account
    ): Promise<Card> {
        if(!card) throw new RequiredResourceNotProvidedError("card");
        if(!account) throw new RequiredResourceNotProvidedError("account");

        card.account = account;
        card.user = await account.owner;
        await card.save();

        (await account.cards).push(card);
        await account.save();

        return card;
    }

    @Query(type => Account, { nullable: true })
    public async account(
        @Arg('identity', { nullable: false }) identity: string,
        @Arg('pin', { nullable: false }) pin: string
    ): Promise<Account> {
        let card = await Card.findOne({ identity });
        if(!card) return null;

        if(card.pin != pin) return null;

        return card.account;
    }

    @Query(type => [Card], { nullable: false })
    public async cards(
        @Ctx('user') user: User
    ): Promise<Card[]> {
        return (await user.cards) || [];
    }
}