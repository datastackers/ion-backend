import { Resolver, Query, Ctx } from "@zuu/owl";
import { Account } from '../model/entities/Account';
import { RequiredResourceNotProvidedError } from '../errors/RequiredResourceNotProvidedError';
import { Chart } from '../model/entities/Chart';

@Resolver()
export class VisualisationResolver {

    @Query(returns => Chart)
    public async chartOfAccount(
        @Ctx('account') account: Account
    ) {
        if(!account) throw new RequiredResourceNotProvidedError('account');
        return await (new Chart().fillFromAccount(account));
    }
}