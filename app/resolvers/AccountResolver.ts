import { Resolver, Query, Ctx, Arg, Mutation } from "@zuu/owl";
import { Account } from '../model/entities/Account';
import { User } from '../model/entities/User';
import { RequiredResourceNotProvidedError } from "../errors/RequiredResourceNotProvidedError";
import { Transaction } from '../model/entities/Transaction';
import * as moment from 'moment';

@Resolver()
export class AccountResolver {
    @Query(returns => Account, { nullable: false })
    public async account(
        @Ctx('account') account: Account
    ): Promise<Account> {
        if(!account) throw new RequiredResourceNotProvidedError('account');
        let transactions = await account.transactions;
        transactions = transactions.sort((a, b) => {
            if(moment(a.created).isAfter(moment(b.created))) return 1;
            return -1;
        });
        return account;
    }

    @Query(returns => [Account], { nullable: false })
    public async accounts(
        @Ctx('user') user: User
    ): Promise<Account[]> {
        return (await user.accounts) || [];
    }

    @Mutation(returns => Account, { nullable: false })
    public async newAccount(
        @Ctx('user') user: User,
        @Arg('name', { nullable: false }) name: string,
        @Arg('currency', { nullable: true }) currency?: string
    ): Promise<Account> {
        return (await new Account(name, currency).init(user));
    }

    @Mutation(returns => Account, { nullable: false })
    public async editAccount(
        @Ctx('account') account: Account,
        @Arg('name', { nullable: true }) name?: string
    ): Promise<Account> {
        if(!account) throw new RequiredResourceNotProvidedError('account');
        
        account.name = name || account.name;
        await account.save();

        return account;
    }
}