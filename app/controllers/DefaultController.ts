import { Controller, Get } from "@zuu/mink";

@Controller('/')
export class DefaultController {
    @Get()
    public index() {
        return "Welcome to DataStack Bank!";
    }
}