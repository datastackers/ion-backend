import { Controller, Get } from "@zuu/mink";

@Controller('/alive')
export class AliveController {
    @Get('/wakeup')
    public index() {
        return "ok.";
    }
}