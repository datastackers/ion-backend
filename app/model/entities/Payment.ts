import { Entity, PrimaryGeneratedColumn, BaseEntity, Column, ManyToOne } from "@zuu/ferret";
import { ObjectType, Field, ID } from '@zuu/owl';
import { Lazy } from '../../packages/async/Lazy';
import { Account } from './Account';
import { User } from "./User";
import { Merchant } from './Merchant';

@ObjectType()
@Entity()
export class Payment extends BaseEntity {

    @Field(type => ID)
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Field({ nullable: false })
    @Column({ default: 0 })
    public value: number;

    @Field({ nullable: false })
    @Column({ default: false })
    public finished: boolean;

    @Field(type => Merchant, { nullable: false })
    @ManyToOne(type => Merchant, e => e.payments, { lazy: true })
    public merchant: Lazy<Merchant>;

    @Field(type => Account, { nullable: true })
    @ManyToOne(type => Account, e => e.payments, { lazy: true })
    public account: Lazy<Account>;

    @Field(type => User, { nullable: true })
    @ManyToOne(type => User, e => e.payments, { lazy: true })
    public user: Lazy<User>;

    public constructor(value: number) {
        super();
        this.value = value;
    }

    public async init(merchant: Merchant): Promise<Payment> {
        this.merchant = merchant;
        await this.save();

        (await merchant.payments).push(this);
        await merchant.save();

        return this;
    }

    public async finish(account: Account): Promise<Payment> {
        this.account = account;
        this.user = await account.owner;
        this.finished = true;

        await this.save();

        (await account.payments).push(this);
        (await (await account.owner).payments).push(this);
        
        await account.save();
        await (await account.owner).save();

        return this;
    }
}