import { ObjectType, Field } from '@zuu/owl';
import { Account } from './Account';

import * as moment from "moment";

@ObjectType()
export class Chart {
    @Field(type => [String], { nullable: false })
    public labels: string[] = [];

    @Field(type => [Number], { nullable: false })
    public data: number[] = [];

    public async fillFromAccount(account: Account): Promise<Chart> {
        let transactions = (await account.transactions).sort((a, b) => {
            if(moment(a.created).isAfter(moment(b.created))) return 1;
            return -1;
        });
        for(let transaction of transactions) {
            this.labels.push((transaction.direction < 0 ? 'Payment' : 'Topup') + ' - ' + moment(transaction.created).format("MMMM Do YYYY"));
            this.data.push(transaction.afterBalance);
        }
        return this;
    }
}