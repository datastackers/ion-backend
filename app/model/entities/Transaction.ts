import { Entity, PrimaryGeneratedColumn, BaseEntity, CreateDateColumn, Column, ManyToOne } from '@zuu/ferret';
import { Field, ID, ObjectType } from '@zuu/owl';
import { Lazy } from '../../packages/async/Lazy';
import { Account } from './Account';
import { User } from './User';

@ObjectType()
@Entity()
export class Transaction extends BaseEntity {

    @Field(type => ID)
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Field(type => Date)
    @CreateDateColumn()
    public created: Date;

    @Field({ nullable: false })
    @Column({ default: 0 })
    public direction: number;

    @Field({ nullable: false })
    @Column({ default: 0 })
    public value: number;

    @Field({ nullable: false })
    @Column({ nullable: false })
    public afterBalance: number;

    @Field(type => Account, { nullable: false })
    @ManyToOne(type => Account, e => e.transactions, { lazy: true })
    public account: Lazy<Account>;

    @Field(type => User, { nullable: false })
    @ManyToOne(type => User, e => e.transactions, { lazy: true })
    public user: Lazy<User>;

    public constructor(direction: number, value: number) {
        super();

        this.direction = direction;
        this.value = value;
    }

    public async init(account: Account): Promise<Transaction> {
        this.account = account;
        this.user = await account.owner;

        this.afterBalance = account.value + (this.value * this.direction);
        
        await this.save();

        (await (await account.owner).transactions).push(this);
        (await account.transactions).push(this);

        await (await account.owner).save();
        await account.save();
        
        return this;
    }
}