import { Entity, PrimaryGeneratedColumn, BaseEntity, Column, ManyToOne } from "@zuu/ferret";
import { Field, ID, ObjectType } from '@zuu/owl';
import { Currency } from '../../packages/account/Currency';
import { Lazy } from '../../packages/async/Lazy';
import { Account } from './Account';
import { User } from './User';

@ObjectType()
@Entity()
export class Topup extends BaseEntity {

    @Field(type => ID)
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Field({ nullable: false })
    @Column({ nullable: false, default: 0.0 })
    public value: number;

    @Field({ nullable: false })
    @Column({ default: Currency.EUR })
    public currency: string;

    @Field({ nullable: false })
    @Column({ type: 'varchar', length: 2000, nullable: false })
    public token: string;

    @Field(type => Account, { nullable: false })
    @ManyToOne(type => Account, e => e.topups, { lazy: true })
    public account: Lazy<Account>;

    @Field(type => User, { nullable: false })
    @ManyToOne(type => User, e => e.topups, { lazy: true })
    public user: Lazy<User>;

    public constructor(token: string, value: number, currency: string = Currency.EUR) {
        super();
        this.token = token;
        this.value = value;
        this.currency = currency;
    }

    public async init(account: Account): Promise<Topup> {
        this.account = account;
        this.user = await account.owner;
        
        await this.save();
        
        (await (await account.owner).topups).push(this);
        (await account.topups).push(this);

        await (await account.owner).save();
        await account.save();
        
        return this;
    }
}