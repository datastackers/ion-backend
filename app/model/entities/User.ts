import { BaseEntity, Column, CreateDateColumn, Entity, Index, PrimaryGeneratedColumn, OneToMany, OneToOne } from '@zuu/ferret';
import { Field, ID, ObjectType } from "@zuu/owl";
import { Lazy } from '../../packages/async/Lazy';
import { Account } from './Account';
import { Topup } from './Topup';
import { Transaction } from './Transaction';
import { Payment } from './Payment';
import { Merchant } from './Merchant';
import { Card } from './Card';

@ObjectType()
@Entity()
export class User extends BaseEntity {
    @Field(type => ID)
    @PrimaryGeneratedColumn("uuid")
    public id: string;

    @Field()
    @CreateDateColumn()
    public created: Date;

    @Field()
    @Column({
        unique: true
    })
    public email: string;

    @Column()
    public password: string;

    @Field({ nullable: true })
    @Column({ nullable: true })
    public fullName: string;

    public firstName(): string {
        return this.fullName.split(" ")[0];
    }

    public lastName(): string {
        return this.fullName.split(" ")[1];
    }

    @Column({ nullable: true })
    public customer: string;

    @Field(type => [Account], { nullable: false })
    @OneToMany(type => Account, e => e.owner, { lazy: true })
    public accounts: Lazy<Account[]>;

    @Field(type => [Topup], { nullable: false })
    @OneToMany(type => Topup, e => e.user, { lazy: true })
    public topups: Lazy<Topup[]>;

    @Field(type => [Payment], { nullable: false })
    @OneToMany(type => Payment, e => e.user, { lazy: true })
    public payments: Lazy<Payment[]>;

    @Field(type => [Transaction], { nullable: false })
    @OneToMany(type => Transaction, e => e.user, { lazy: true })
    public transactions: Lazy<Transaction[]>;

    @Field(type => Merchant, { nullable: true })
    @OneToOne(type => Merchant, e => e.user, { lazy: true })
    public merchant: Lazy<Merchant>;

    @Field(type => [Card], { nullable: false })
    @OneToMany(type => Card, e => e.user, { lazy: true })
    public cards: Lazy<Card[]>;

    public constructor(email: string, password: string) {
        super();
        
        this.email = email;
        this.password = password;
    }
}