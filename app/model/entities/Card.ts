import { Entity, PrimaryGeneratedColumn, BaseEntity, Column, ManyToOne } from '@zuu/ferret';
import { Field, ID, ObjectType } from '@zuu/owl';
import { Lazy } from '../../packages/async/Lazy';
import { Account } from './Account';
import { User } from './User';

@ObjectType()
@Entity()
export class Card extends BaseEntity {

    @Field(type => ID)
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Field(type => String, { nullable: false })
    public code(): string{
        let pieces = this.id.split("-");
        return pieces[pieces.length - 1];
    }

    @Column({ nullable: false })
    public pin: string;

    @Field({ nullable: false })
    @Column({ nullable: false })
    public identity: string;

    @Field(type => Account, { nullable: true })
    @ManyToOne(type => Account, e => e.cards, { lazy: true })
    public account: Lazy<Account>;

    @Field(type => User, { nullable: true })
    @ManyToOne(type => User, e => e.cards, { lazy: true })
    public user: Lazy<User>;

    public constructor(){
        super();
        this.pin = "" + Math.floor(1000 + Math.random() * 8999);
    }

    public async pair(user: User) {
        this.user = user;
        await this.save();

        (await user.cards).push(this);
        await user.save();

        return this;
    }
}