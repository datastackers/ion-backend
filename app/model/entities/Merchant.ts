import { Entity, PrimaryGeneratedColumn, BaseEntity, OneToMany, OneToOne, Column, JoinColumn } from '@zuu/ferret';
import { ObjectType, Field, ID } from '@zuu/owl';
import { Lazy } from '../../packages/async/Lazy';
import { User } from './User';
import { Payment } from './Payment';

@ObjectType()
@Entity()
export class Merchant extends BaseEntity {

    @Field(type => ID)
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Field({ nullable: false })
    @Column({ nullable: false })
    public name: string;

    @Field(type => [Payment], { nullable: false })
    @OneToMany(type => Payment, e => e.merchant, { lazy: true })
    public payments: Lazy<Payment[]>;

    @Field(type => User, { nullable: false })
    @OneToOne(type => User, e => e.merchant, { lazy: true })
    @JoinColumn()
    public user: Lazy<User>;

    public constructor(name: string) {
        super();
        this.name = name;
    }
    
    public async init(user: User): Promise<Merchant> {
        this.user = user;
        await this.save();
        
        user.merchant = this;
        await user.save();
        
        return this;       
    }
}