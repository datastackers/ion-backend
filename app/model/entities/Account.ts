import { Entity, PrimaryGeneratedColumn, BaseEntity, Column, ManyToOne, OneToMany } from "@zuu/ferret";
import { Currency } from '../../packages/account/Currency';
import { ObjectType, Field, ID, Float } from '@zuu/owl';
import { Lazy } from '../../packages/async/Lazy';
import { User } from "./User";
import { Topup } from "./Topup";
import { Transaction } from "./Transaction";
import { Payment } from './Payment';
import { Card } from './Card';

@ObjectType()
@Entity()
export class Account extends BaseEntity {
    @Field(type => ID)
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Field({ nullable: false })
    @Column({ nullable: false })
    public name: string;

    @Field({ nullable: false })
    @Column({ default: Currency.EUR })
    public currency: string;

    @Field({ nullable: false })
    @Column({ default: 0.0 })
    public value: number;

    @Field(type => User, { nullable: false })
    @ManyToOne(type => User, e => e.accounts, { lazy: true })
    public owner: Lazy<User>;

    @Field(type => [Topup], { nullable: false })
    @OneToMany(type => Topup, e => e.account, { lazy: true })
    public topups: Lazy<Topup[]>;

    @Field(type => [Payment], { nullable: true })
    @OneToMany(type => Payment, e => e.account, { lazy: true })
    public payments: Lazy<Payment[]>;

    @Field(type => [Transaction], { nullable: false })
    @OneToMany(type => Transaction, e => e.account, { lazy: true })
    public transactions: Lazy<Transaction[]>;

    @Field(type => [Card], { nullable: false })
    @OneToMany(type => Card, e => e.account, { lazy: true })
    public cards: Lazy<Card[]>;

    public constructor(name: string, currency: (string | Currency) = Currency.EUR) {
        super();
        this.name = name;
        this.currency = currency;
    }

    public async init(user: User): Promise<Account> {
        this.owner = user;
        await this.save();
        
        (await user.accounts).push(this);
        await user.save();

        return this;
    }
}