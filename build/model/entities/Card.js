"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ferret_1 = require("@zuu/ferret");
const owl_1 = require("@zuu/owl");
const Account_1 = require("./Account");
const User_1 = require("./User");
let Card = class Card extends ferret_1.BaseEntity {
    constructor() {
        super();
        this.pin = "" + Math.floor(1000 + Math.random() * 8999);
    }
    code() {
        let pieces = this.id.split("-");
        return pieces[pieces.length - 1];
    }
    pair(user) {
        return __awaiter(this, void 0, void 0, function* () {
            this.user = user;
            yield this.save();
            (yield user.cards).push(this);
            yield user.save();
            return this;
        });
    }
};
__decorate([
    owl_1.Field(type => owl_1.ID),
    ferret_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Card.prototype, "id", void 0);
__decorate([
    owl_1.Field(type => String, { nullable: false }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", String)
], Card.prototype, "code", null);
__decorate([
    ferret_1.Column({ nullable: false }),
    __metadata("design:type", String)
], Card.prototype, "pin", void 0);
__decorate([
    owl_1.Field({ nullable: false }),
    ferret_1.Column({ nullable: false }),
    __metadata("design:type", String)
], Card.prototype, "identity", void 0);
__decorate([
    owl_1.Field(type => Account_1.Account, { nullable: true }),
    ferret_1.ManyToOne(type => Account_1.Account, e => e.cards, { lazy: true }),
    __metadata("design:type", Object)
], Card.prototype, "account", void 0);
__decorate([
    owl_1.Field(type => User_1.User, { nullable: true }),
    ferret_1.ManyToOne(type => User_1.User, e => e.cards, { lazy: true }),
    __metadata("design:type", Object)
], Card.prototype, "user", void 0);
Card = __decorate([
    owl_1.ObjectType(),
    ferret_1.Entity(),
    __metadata("design:paramtypes", [])
], Card);
exports.Card = Card;
//# sourceMappingURL=Card.js.map