"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ferret_1 = require("@zuu/ferret");
const owl_1 = require("@zuu/owl");
const Currency_1 = require("../../packages/account/Currency");
const Account_1 = require("./Account");
const User_1 = require("./User");
let Topup = class Topup extends ferret_1.BaseEntity {
    constructor(token, value, currency = Currency_1.Currency.EUR) {
        super();
        this.token = token;
        this.value = value;
        this.currency = currency;
    }
    init(account) {
        return __awaiter(this, void 0, void 0, function* () {
            this.account = account;
            this.user = yield account.owner;
            yield this.save();
            (yield (yield account.owner).topups).push(this);
            (yield account.topups).push(this);
            yield (yield account.owner).save();
            yield account.save();
            return this;
        });
    }
};
__decorate([
    owl_1.Field(type => owl_1.ID),
    ferret_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Topup.prototype, "id", void 0);
__decorate([
    owl_1.Field({ nullable: false }),
    ferret_1.Column({ nullable: false, default: 0.0 }),
    __metadata("design:type", Number)
], Topup.prototype, "value", void 0);
__decorate([
    owl_1.Field({ nullable: false }),
    ferret_1.Column({ default: Currency_1.Currency.EUR }),
    __metadata("design:type", String)
], Topup.prototype, "currency", void 0);
__decorate([
    owl_1.Field({ nullable: false }),
    ferret_1.Column({ type: 'varchar', length: 2000, nullable: false }),
    __metadata("design:type", String)
], Topup.prototype, "token", void 0);
__decorate([
    owl_1.Field(type => Account_1.Account, { nullable: false }),
    ferret_1.ManyToOne(type => Account_1.Account, e => e.topups, { lazy: true }),
    __metadata("design:type", Object)
], Topup.prototype, "account", void 0);
__decorate([
    owl_1.Field(type => User_1.User, { nullable: false }),
    ferret_1.ManyToOne(type => User_1.User, e => e.topups, { lazy: true }),
    __metadata("design:type", Object)
], Topup.prototype, "user", void 0);
Topup = __decorate([
    owl_1.ObjectType(),
    ferret_1.Entity(),
    __metadata("design:paramtypes", [String, Number, String])
], Topup);
exports.Topup = Topup;
//# sourceMappingURL=Topup.js.map