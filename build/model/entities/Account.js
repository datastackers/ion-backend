"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ferret_1 = require("@zuu/ferret");
const Currency_1 = require("../../packages/account/Currency");
const owl_1 = require("@zuu/owl");
const User_1 = require("./User");
const Topup_1 = require("./Topup");
const Transaction_1 = require("./Transaction");
const Payment_1 = require("./Payment");
const Card_1 = require("./Card");
let Account = class Account extends ferret_1.BaseEntity {
    constructor(name, currency = Currency_1.Currency.EUR) {
        super();
        this.name = name;
        this.currency = currency;
    }
    init(user) {
        return __awaiter(this, void 0, void 0, function* () {
            this.owner = user;
            yield this.save();
            (yield user.accounts).push(this);
            yield user.save();
            return this;
        });
    }
};
__decorate([
    owl_1.Field(type => owl_1.ID),
    ferret_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Account.prototype, "id", void 0);
__decorate([
    owl_1.Field({ nullable: false }),
    ferret_1.Column({ nullable: false }),
    __metadata("design:type", String)
], Account.prototype, "name", void 0);
__decorate([
    owl_1.Field({ nullable: false }),
    ferret_1.Column({ default: Currency_1.Currency.EUR }),
    __metadata("design:type", String)
], Account.prototype, "currency", void 0);
__decorate([
    owl_1.Field({ nullable: false }),
    ferret_1.Column({ default: 0.0 }),
    __metadata("design:type", Number)
], Account.prototype, "value", void 0);
__decorate([
    owl_1.Field(type => User_1.User, { nullable: false }),
    ferret_1.ManyToOne(type => User_1.User, e => e.accounts, { lazy: true }),
    __metadata("design:type", Object)
], Account.prototype, "owner", void 0);
__decorate([
    owl_1.Field(type => [Topup_1.Topup], { nullable: false }),
    ferret_1.OneToMany(type => Topup_1.Topup, e => e.account, { lazy: true }),
    __metadata("design:type", Object)
], Account.prototype, "topups", void 0);
__decorate([
    owl_1.Field(type => [Payment_1.Payment], { nullable: true }),
    ferret_1.OneToMany(type => Payment_1.Payment, e => e.account, { lazy: true }),
    __metadata("design:type", Object)
], Account.prototype, "payments", void 0);
__decorate([
    owl_1.Field(type => [Transaction_1.Transaction], { nullable: false }),
    ferret_1.OneToMany(type => Transaction_1.Transaction, e => e.account, { lazy: true }),
    __metadata("design:type", Object)
], Account.prototype, "transactions", void 0);
__decorate([
    owl_1.Field(type => [Card_1.Card], { nullable: false }),
    ferret_1.OneToMany(type => Card_1.Card, e => e.account, { lazy: true }),
    __metadata("design:type", Object)
], Account.prototype, "cards", void 0);
Account = __decorate([
    owl_1.ObjectType(),
    ferret_1.Entity(),
    __metadata("design:paramtypes", [String, String])
], Account);
exports.Account = Account;
//# sourceMappingURL=Account.js.map