"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ferret_1 = require("@zuu/ferret");
const owl_1 = require("@zuu/owl");
const Account_1 = require("./Account");
const User_1 = require("./User");
let Transaction = class Transaction extends ferret_1.BaseEntity {
    constructor(direction, value) {
        super();
        this.direction = direction;
        this.value = value;
    }
    init(account) {
        return __awaiter(this, void 0, void 0, function* () {
            this.account = account;
            this.user = yield account.owner;
            this.afterBalance = account.value + (this.value * this.direction);
            yield this.save();
            (yield (yield account.owner).transactions).push(this);
            (yield account.transactions).push(this);
            yield (yield account.owner).save();
            yield account.save();
            return this;
        });
    }
};
__decorate([
    owl_1.Field(type => owl_1.ID),
    ferret_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Transaction.prototype, "id", void 0);
__decorate([
    owl_1.Field(type => Date),
    ferret_1.CreateDateColumn(),
    __metadata("design:type", Date)
], Transaction.prototype, "created", void 0);
__decorate([
    owl_1.Field({ nullable: false }),
    ferret_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], Transaction.prototype, "direction", void 0);
__decorate([
    owl_1.Field({ nullable: false }),
    ferret_1.Column({ default: 0 }),
    __metadata("design:type", Number)
], Transaction.prototype, "value", void 0);
__decorate([
    owl_1.Field({ nullable: false }),
    ferret_1.Column({ nullable: false }),
    __metadata("design:type", Number)
], Transaction.prototype, "afterBalance", void 0);
__decorate([
    owl_1.Field(type => Account_1.Account, { nullable: false }),
    ferret_1.ManyToOne(type => Account_1.Account, e => e.transactions, { lazy: true }),
    __metadata("design:type", Object)
], Transaction.prototype, "account", void 0);
__decorate([
    owl_1.Field(type => User_1.User, { nullable: false }),
    ferret_1.ManyToOne(type => User_1.User, e => e.transactions, { lazy: true }),
    __metadata("design:type", Object)
], Transaction.prototype, "user", void 0);
Transaction = __decorate([
    owl_1.ObjectType(),
    ferret_1.Entity(),
    __metadata("design:paramtypes", [Number, Number])
], Transaction);
exports.Transaction = Transaction;
//# sourceMappingURL=Transaction.js.map