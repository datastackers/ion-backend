"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const owl_1 = require("@zuu/owl");
const moment = require("moment");
let Chart = class Chart {
    constructor() {
        this.labels = [];
        this.data = [];
    }
    fillFromAccount(account) {
        return __awaiter(this, void 0, void 0, function* () {
            let transactions = (yield account.transactions).sort((a, b) => {
                if (moment(a.created).isAfter(moment(b.created)))
                    return 1;
                return -1;
            });
            for (let transaction of transactions) {
                this.labels.push((transaction.direction < 0 ? 'Payment' : 'Topup') + ' - ' + moment(transaction.created).format("MMMM Do YYYY"));
                this.data.push(transaction.afterBalance);
            }
            return this;
        });
    }
};
__decorate([
    owl_1.Field(type => [String], { nullable: false }),
    __metadata("design:type", Array)
], Chart.prototype, "labels", void 0);
__decorate([
    owl_1.Field(type => [Number], { nullable: false }),
    __metadata("design:type", Array)
], Chart.prototype, "data", void 0);
Chart = __decorate([
    owl_1.ObjectType()
], Chart);
exports.Chart = Chart;
//# sourceMappingURL=Chart.js.map