"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const ferret_1 = require("@zuu/ferret");
const owl_1 = require("@zuu/owl");
const Account_1 = require("./Account");
const Topup_1 = require("./Topup");
const Transaction_1 = require("./Transaction");
const Payment_1 = require("./Payment");
const Merchant_1 = require("./Merchant");
const Card_1 = require("./Card");
let User = class User extends ferret_1.BaseEntity {
    constructor(email, password) {
        super();
        this.email = email;
        this.password = password;
    }
    firstName() {
        return this.fullName.split(" ")[0];
    }
    lastName() {
        return this.fullName.split(" ")[1];
    }
};
__decorate([
    owl_1.Field(type => owl_1.ID),
    ferret_1.PrimaryGeneratedColumn("uuid"),
    __metadata("design:type", String)
], User.prototype, "id", void 0);
__decorate([
    owl_1.Field(),
    ferret_1.CreateDateColumn(),
    __metadata("design:type", Date)
], User.prototype, "created", void 0);
__decorate([
    owl_1.Field(),
    ferret_1.Column({
        unique: true
    }),
    __metadata("design:type", String)
], User.prototype, "email", void 0);
__decorate([
    ferret_1.Column(),
    __metadata("design:type", String)
], User.prototype, "password", void 0);
__decorate([
    owl_1.Field({ nullable: true }),
    ferret_1.Column({ nullable: true }),
    __metadata("design:type", String)
], User.prototype, "fullName", void 0);
__decorate([
    ferret_1.Column({ nullable: true }),
    __metadata("design:type", String)
], User.prototype, "customer", void 0);
__decorate([
    owl_1.Field(type => [Account_1.Account], { nullable: false }),
    ferret_1.OneToMany(type => Account_1.Account, e => e.owner, { lazy: true }),
    __metadata("design:type", Object)
], User.prototype, "accounts", void 0);
__decorate([
    owl_1.Field(type => [Topup_1.Topup], { nullable: false }),
    ferret_1.OneToMany(type => Topup_1.Topup, e => e.user, { lazy: true }),
    __metadata("design:type", Object)
], User.prototype, "topups", void 0);
__decorate([
    owl_1.Field(type => [Payment_1.Payment], { nullable: false }),
    ferret_1.OneToMany(type => Payment_1.Payment, e => e.user, { lazy: true }),
    __metadata("design:type", Object)
], User.prototype, "payments", void 0);
__decorate([
    owl_1.Field(type => [Transaction_1.Transaction], { nullable: false }),
    ferret_1.OneToMany(type => Transaction_1.Transaction, e => e.user, { lazy: true }),
    __metadata("design:type", Object)
], User.prototype, "transactions", void 0);
__decorate([
    owl_1.Field(type => Merchant_1.Merchant, { nullable: true }),
    ferret_1.OneToOne(type => Merchant_1.Merchant, e => e.user, { lazy: true }),
    __metadata("design:type", Object)
], User.prototype, "merchant", void 0);
__decorate([
    owl_1.Field(type => [Card_1.Card], { nullable: false }),
    ferret_1.OneToMany(type => Card_1.Card, e => e.user, { lazy: true }),
    __metadata("design:type", Object)
], User.prototype, "cards", void 0);
User = __decorate([
    owl_1.ObjectType(),
    ferret_1.Entity(),
    __metadata("design:paramtypes", [String, String])
], User);
exports.User = User;
//# sourceMappingURL=User.js.map