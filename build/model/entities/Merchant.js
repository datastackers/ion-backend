"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ferret_1 = require("@zuu/ferret");
const owl_1 = require("@zuu/owl");
const User_1 = require("./User");
const Payment_1 = require("./Payment");
let Merchant = class Merchant extends ferret_1.BaseEntity {
    constructor(name) {
        super();
        this.name = name;
    }
    init(user) {
        return __awaiter(this, void 0, void 0, function* () {
            this.user = user;
            yield this.save();
            user.merchant = this;
            yield user.save();
            return this;
        });
    }
};
__decorate([
    owl_1.Field(type => owl_1.ID),
    ferret_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Merchant.prototype, "id", void 0);
__decorate([
    owl_1.Field({ nullable: false }),
    ferret_1.Column({ nullable: false }),
    __metadata("design:type", String)
], Merchant.prototype, "name", void 0);
__decorate([
    owl_1.Field(type => [Payment_1.Payment], { nullable: false }),
    ferret_1.OneToMany(type => Payment_1.Payment, e => e.merchant, { lazy: true }),
    __metadata("design:type", Object)
], Merchant.prototype, "payments", void 0);
__decorate([
    owl_1.Field(type => User_1.User, { nullable: false }),
    ferret_1.OneToOne(type => User_1.User, e => e.merchant, { lazy: true }),
    ferret_1.JoinColumn(),
    __metadata("design:type", Object)
], Merchant.prototype, "user", void 0);
Merchant = __decorate([
    owl_1.ObjectType(),
    ferret_1.Entity(),
    __metadata("design:paramtypes", [String])
], Merchant);
exports.Merchant = Merchant;
//# sourceMappingURL=Merchant.js.map