"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class LoginSuccessResponse {
    constructor(user, token, refresh) {
        this.email = user.email;
        this.id = user.id;
        this.token = token.chars;
        this.refresh = refresh.chars;
    }
}
exports.LoginSuccessResponse = LoginSuccessResponse;
//# sourceMappingURL=LoginSuccessResponse.js.map