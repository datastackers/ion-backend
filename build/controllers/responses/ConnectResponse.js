"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ConnectResponse {
    constructor(user) {
        this.id = user.id;
        this.email = user.email;
        this.fullName = user.fullName;
    }
}
exports.ConnectResponse = ConnectResponse;
//# sourceMappingURL=ConnectResponse.js.map