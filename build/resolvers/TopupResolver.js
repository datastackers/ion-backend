"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const owl_1 = require("@zuu/owl");
const Topup_1 = require("../model/entities/Topup");
const User_1 = require("../model/entities/User");
const Braintree_1 = require("../packages/braintree/Braintree");
const vet_1 = require("@zuu/vet");
const Account_1 = require("../model/entities/Account");
const RequiredResourceNotProvidedError_1 = require("../errors/RequiredResourceNotProvidedError");
const Transaction_1 = require("../model/entities/Transaction");
let TopupResolver = class TopupResolver {
    beginTopup(user, account, value, currency) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!account)
                throw new RequiredResourceNotProvidedError_1.RequiredResourceNotProvidedError('account');
            let token = yield this.braintree.clientToken(user.customer);
            let topup = new Topup_1.Topup(token, value, currency).init(account);
            return topup;
        });
    }
    finishTopup(topup, nonce) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!topup)
                throw new RequiredResourceNotProvidedError_1.RequiredResourceNotProvidedError('topup');
            let result = yield this.braintree.sale(topup.value + "", topup.currency, nonce);
            if (result) {
                yield new Transaction_1.Transaction(1, topup.value).init(yield topup.account);
                (yield topup.account).value += topup.value;
                yield (yield topup.account).save();
            }
            return topup;
        });
    }
};
__decorate([
    vet_1.Inject,
    __metadata("design:type", Braintree_1.Braintree)
], TopupResolver.prototype, "braintree", void 0);
__decorate([
    owl_1.Mutation(returns => Topup_1.Topup, { nullable: false }),
    __param(0, owl_1.Ctx('user')),
    __param(1, owl_1.Ctx('account')),
    __param(2, owl_1.Arg('value', type => owl_1.Float, { nullable: false })),
    __param(3, owl_1.Arg('currency', { nullable: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [User_1.User,
        Account_1.Account, Number, String]),
    __metadata("design:returntype", Promise)
], TopupResolver.prototype, "beginTopup", null);
__decorate([
    owl_1.Mutation(returns => Topup_1.Topup, { nullable: false }),
    __param(0, owl_1.Ctx('topup')),
    __param(1, owl_1.Arg('nonce', { nullable: false })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Topup_1.Topup, String]),
    __metadata("design:returntype", Promise)
], TopupResolver.prototype, "finishTopup", null);
TopupResolver = __decorate([
    owl_1.Resolver()
], TopupResolver);
exports.TopupResolver = TopupResolver;
//# sourceMappingURL=TopupResolver.js.map