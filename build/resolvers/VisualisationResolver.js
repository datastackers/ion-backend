"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const owl_1 = require("@zuu/owl");
const Account_1 = require("../model/entities/Account");
const RequiredResourceNotProvidedError_1 = require("../errors/RequiredResourceNotProvidedError");
const Chart_1 = require("../model/entities/Chart");
let VisualisationResolver = class VisualisationResolver {
    chartOfAccount(account) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!account)
                throw new RequiredResourceNotProvidedError_1.RequiredResourceNotProvidedError('account');
            return yield (new Chart_1.Chart().fillFromAccount(account));
        });
    }
};
__decorate([
    owl_1.Query(returns => Chart_1.Chart),
    __param(0, owl_1.Ctx('account')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Account_1.Account]),
    __metadata("design:returntype", Promise)
], VisualisationResolver.prototype, "chartOfAccount", null);
VisualisationResolver = __decorate([
    owl_1.Resolver()
], VisualisationResolver);
exports.VisualisationResolver = VisualisationResolver;
//# sourceMappingURL=VisualisationResolver.js.map