"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const owl_1 = require("@zuu/owl");
const Payment_1 = require("../model/entities/Payment");
const Merchant_1 = require("../model/entities/Merchant");
const RequiredResourceNotProvidedError_1 = require("../errors/RequiredResourceNotProvidedError");
const Account_1 = require("../model/entities/Account");
const Transaction_1 = require("../model/entities/Transaction");
let PaymentResolver = class PaymentResolver {
    create(merchant, value) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!merchant)
                throw new RequiredResourceNotProvidedError_1.RequiredResourceNotProvidedError("merchant");
            return yield (new Payment_1.Payment(value).init(merchant));
        });
    }
    pay(payment, account) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!payment)
                throw new RequiredResourceNotProvidedError_1.RequiredResourceNotProvidedError("payment");
            if (!account)
                throw new RequiredResourceNotProvidedError_1.RequiredResourceNotProvidedError("account");
            if (account.value < payment.value)
                return null;
            yield (new Transaction_1.Transaction(-1, payment.value).init(account));
            account.value -= payment.value;
            yield account.save();
            yield payment.finish(account);
            return payment;
        });
    }
    payment(payment) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!payment)
                throw new RequiredResourceNotProvidedError_1.RequiredResourceNotProvidedError("payment");
            return payment;
        });
    }
};
__decorate([
    owl_1.Mutation(returns => Payment_1.Payment, { nullable: false }),
    __param(0, owl_1.Ctx("merchant")),
    __param(1, owl_1.Arg("value", { nullable: false })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Merchant_1.Merchant, Number]),
    __metadata("design:returntype", Promise)
], PaymentResolver.prototype, "create", null);
__decorate([
    owl_1.Mutation(returns => Payment_1.Payment, { nullable: true }),
    __param(0, owl_1.Ctx("payment")),
    __param(1, owl_1.Ctx("account")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Payment_1.Payment,
        Account_1.Account]),
    __metadata("design:returntype", Promise)
], PaymentResolver.prototype, "pay", null);
__decorate([
    owl_1.Query(type => Payment_1.Payment),
    __param(0, owl_1.Ctx("payment")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Payment_1.Payment]),
    __metadata("design:returntype", Promise)
], PaymentResolver.prototype, "payment", null);
PaymentResolver = __decorate([
    owl_1.Resolver()
], PaymentResolver);
exports.PaymentResolver = PaymentResolver;
//# sourceMappingURL=PaymentResolver.js.map