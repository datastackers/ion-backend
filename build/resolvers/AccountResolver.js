"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const owl_1 = require("@zuu/owl");
const Account_1 = require("../model/entities/Account");
const User_1 = require("../model/entities/User");
const RequiredResourceNotProvidedError_1 = require("../errors/RequiredResourceNotProvidedError");
const moment = require("moment");
let AccountResolver = class AccountResolver {
    account(account) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!account)
                throw new RequiredResourceNotProvidedError_1.RequiredResourceNotProvidedError('account');
            let transactions = yield account.transactions;
            transactions = transactions.sort((a, b) => {
                if (moment(a.created).isAfter(moment(b.created)))
                    return 1;
                return -1;
            });
            return account;
        });
    }
    accounts(user) {
        return __awaiter(this, void 0, void 0, function* () {
            return (yield user.accounts) || [];
        });
    }
    newAccount(user, name, currency) {
        return __awaiter(this, void 0, void 0, function* () {
            return (yield new Account_1.Account(name, currency).init(user));
        });
    }
    editAccount(account, name) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!account)
                throw new RequiredResourceNotProvidedError_1.RequiredResourceNotProvidedError('account');
            account.name = name || account.name;
            yield account.save();
            return account;
        });
    }
};
__decorate([
    owl_1.Query(returns => Account_1.Account, { nullable: false }),
    __param(0, owl_1.Ctx('account')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Account_1.Account]),
    __metadata("design:returntype", Promise)
], AccountResolver.prototype, "account", null);
__decorate([
    owl_1.Query(returns => [Account_1.Account], { nullable: false }),
    __param(0, owl_1.Ctx('user')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [User_1.User]),
    __metadata("design:returntype", Promise)
], AccountResolver.prototype, "accounts", null);
__decorate([
    owl_1.Mutation(returns => Account_1.Account, { nullable: false }),
    __param(0, owl_1.Ctx('user')),
    __param(1, owl_1.Arg('name', { nullable: false })),
    __param(2, owl_1.Arg('currency', { nullable: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [User_1.User, String, String]),
    __metadata("design:returntype", Promise)
], AccountResolver.prototype, "newAccount", null);
__decorate([
    owl_1.Mutation(returns => Account_1.Account, { nullable: false }),
    __param(0, owl_1.Ctx('account')),
    __param(1, owl_1.Arg('name', { nullable: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Account_1.Account, String]),
    __metadata("design:returntype", Promise)
], AccountResolver.prototype, "editAccount", null);
AccountResolver = __decorate([
    owl_1.Resolver()
], AccountResolver);
exports.AccountResolver = AccountResolver;
//# sourceMappingURL=AccountResolver.js.map