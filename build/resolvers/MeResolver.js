"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const owl_1 = require("@zuu/owl");
const User_1 = require("../model/entities/User");
const vet_1 = require("@zuu/vet");
const Braintree_1 = require("../packages/braintree/Braintree");
const Account_1 = require("../model/entities/Account");
const Currency_1 = require("../packages/account/Currency");
let MeResolver = class MeResolver {
    me(user) {
        return __awaiter(this, void 0, void 0, function* () {
            return user;
        });
    }
    update(user, fullName) {
        return __awaiter(this, void 0, void 0, function* () {
            user.fullName = fullName;
            user.customer = yield this.braintree.createCustomer({ email: user.email, firstName: user.firstName(), lastName: user.lastName() });
            yield user.save();
            yield (new Account_1.Account(user.firstName() + "'s EUR Account", Currency_1.Currency.EUR).init(user));
            yield user.save();
            return user;
        });
    }
};
__decorate([
    vet_1.Inject,
    __metadata("design:type", Braintree_1.Braintree)
], MeResolver.prototype, "braintree", void 0);
__decorate([
    owl_1.Query(returns => User_1.User, { nullable: false }),
    __param(0, owl_1.Ctx("user")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [User_1.User]),
    __metadata("design:returntype", Promise)
], MeResolver.prototype, "me", null);
__decorate([
    owl_1.Mutation(returns => User_1.User, { nullable: false }),
    __param(0, owl_1.Ctx("user")),
    __param(1, owl_1.Arg("fullName", { nullable: false })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [User_1.User, String]),
    __metadata("design:returntype", Promise)
], MeResolver.prototype, "update", null);
MeResolver = __decorate([
    owl_1.Resolver()
], MeResolver);
exports.MeResolver = MeResolver;
//# sourceMappingURL=MeResolver.js.map