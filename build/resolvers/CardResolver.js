"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const owl_1 = require("@zuu/owl");
const Card_1 = require("../model/entities/Card");
const User_1 = require("../model/entities/User");
const RequiredResourceNotProvidedError_1 = require("../errors/RequiredResourceNotProvidedError");
const Account_1 = require("../model/entities/Account");
let CardResolver = class CardResolver {
    pair(user, card) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!card)
                throw new RequiredResourceNotProvidedError_1.RequiredResourceNotProvidedError("card");
            yield card.pair(user);
            return card;
        });
    }
    allocate(card, account) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!card)
                throw new RequiredResourceNotProvidedError_1.RequiredResourceNotProvidedError("card");
            if (!account)
                throw new RequiredResourceNotProvidedError_1.RequiredResourceNotProvidedError("account");
            card.account = account;
            card.user = yield account.owner;
            yield card.save();
            (yield account.cards).push(card);
            yield account.save();
            return card;
        });
    }
    account(identity, pin) {
        return __awaiter(this, void 0, void 0, function* () {
            let card = yield Card_1.Card.findOne({ identity });
            if (!card)
                return null;
            if (card.pin != pin)
                return null;
            return card.account;
        });
    }
    cards(user) {
        return __awaiter(this, void 0, void 0, function* () {
            return (yield user.cards) || [];
        });
    }
};
__decorate([
    owl_1.Mutation(returns => Card_1.Card, { nullable: false }),
    __param(0, owl_1.Ctx('user')),
    __param(1, owl_1.Ctx('card')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [User_1.User,
        Card_1.Card]),
    __metadata("design:returntype", Promise)
], CardResolver.prototype, "pair", null);
__decorate([
    owl_1.Mutation(returns => Card_1.Card, { nullable: true }),
    __param(0, owl_1.Ctx('card')),
    __param(1, owl_1.Ctx('account')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Card_1.Card,
        Account_1.Account]),
    __metadata("design:returntype", Promise)
], CardResolver.prototype, "allocate", null);
__decorate([
    owl_1.Query(type => Account_1.Account, { nullable: true }),
    __param(0, owl_1.Arg('identity', { nullable: false })),
    __param(1, owl_1.Arg('pin', { nullable: false })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", Promise)
], CardResolver.prototype, "account", null);
__decorate([
    owl_1.Query(type => [Card_1.Card], { nullable: false }),
    __param(0, owl_1.Ctx('user')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [User_1.User]),
    __metadata("design:returntype", Promise)
], CardResolver.prototype, "cards", null);
CardResolver = __decorate([
    owl_1.Resolver()
], CardResolver);
exports.CardResolver = CardResolver;
//# sourceMappingURL=CardResolver.js.map