"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const bootstrap_1 = require("@zuu/bootstrap");
const vet_1 = require("@zuu/vet");
const Timer_1 = require("./packages/timer/Timer");
const AliveController_1 = require("./controllers/AliveController");
const DefaultController_1 = require("./controllers/DefaultController");
const ListeningEventListener_1 = require("./listeners/ListeningEventListener");
const CardResolver_1 = require("./resolvers/CardResolver");
vet_1.Debugger.deafults();
let tag = vet_1.Debugger.tag("service-default");
let options = {
    server: {
        port: parseInt(process.env["PORT"]) || 4100,
        modules: []
    },
    listeners: [
        new ListeningEventListener_1.ListeningEventListener
    ],
    controllers: [
        AliveController_1.AliveController,
        DefaultController_1.DefaultController
    ],
    cors: true
};
let timer = new Timer_1.Timer().reset();
vet_1.Runtime.scoped(null, (_) => __awaiter(this, void 0, void 0, function* () {
    vet_1.Debugger.log(tag `Initialization began!`);
    let { app } = yield bootstrap_1.Bootstrap.scope(options).run();
    return (typeof app != "undefined" && app != null);
}))
    .then((result) => __awaiter(this, void 0, void 0, function* () {
    vet_1.Debugger.log(tag `Initialization succeeded! Took ${timer.stop().diff()}ms!`);
    // let card = new Card();
    // card.identity = "BC:EF:CB:FE";
    // await card.save();
    let resolver = new CardResolver_1.CardResolver();
    // let user = await User.findOne({ where: { email: "test10@dev.com" } })
    // console.log(user);
    // let account = (await user.accounts)[0];
    // account.value = 300;
    // await account.save();
    // let cards = await Card.find();
    // for(let card of cards) {
    //     await resolver.allocate(card, account);
    // }
    // await resolver.allocate(card, account);
    // let card = await Card.findOne({where: {identity: "AB:BA:AB:BA"}});
    // card.identity = "DF:88:C5:A4";
    // await card.save();
}))
    .catch(vet_1.Debugger.error);
//# sourceMappingURL=index.js.map