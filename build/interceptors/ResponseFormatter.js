"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const mink_1 = require("@zuu/mink");
const vet_1 = require("@zuu/vet");
let tag = vet_1.Debugger.tag("response-formatter-interceptor");
let ResponseFormatter = class ResponseFormatter {
    constructor() {
        vet_1.Debugger.log(tag `Interceptor loaded!`);
    }
    intercept(action, result) {
        action.request.time.end = Date.now();
        action.request.time.spent = Date.now() - action.request.time.start;
        if (!result.data) {
            result = { data: result };
        }
        for (let headerId in action.request.headers) {
            if (headerId.indexOf("x-appengine") != -1) {
                delete action.request.headers[headerId];
            }
        }
        result.meta = {
            headers: action.request.headers,
            body: action.request.body,
            query: action.request.query,
            params: action.request.params,
            time: {
                now: Date.now(),
                start: action.request.time.start,
                spent: action.request.time.spent
            }
        };
        return result;
    }
};
ResponseFormatter = __decorate([
    mink_1.Interceptor(),
    __metadata("design:paramtypes", [])
], ResponseFormatter);
exports.ResponseFormatter = ResponseFormatter;
//# sourceMappingURL=ResponseFormatter.js.map