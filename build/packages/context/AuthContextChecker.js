"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const User_1 = require("../../model/entities/User");
const Tokenize_1 = require("../tokens/Tokenize");
const TokenScope_1 = require("../tokens/TokenScope");
const InvalidTokenError_1 = require("../../errors/InvalidTokenError");
const AccountDoesNotExistError_1 = require("../../errors/AccountDoesNotExistError");
class AuthContextChecker {
    static check(token) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!token)
                return null;
            let authToken = yield Tokenize_1.Tokenize.instance().find(token, TokenScope_1.TokenScope.AUTH);
            if (!authToken)
                throw new InvalidTokenError_1.InvalidTokenError(token);
            let user = yield User_1.User.findOne(authToken.target);
            if (!user)
                throw new AccountDoesNotExistError_1.AccountDoesNotExistError(authToken.target);
            return user;
        });
    }
}
exports.AuthContextChecker = AuthContextChecker;
//# sourceMappingURL=AuthContextChecker.js.map