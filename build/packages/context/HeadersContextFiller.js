"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const UnresolvableResourceAccessError_1 = require("../../errors/UnresolvableResourceAccessError");
const Account_1 = require("../../model/entities/Account");
const Topup_1 = require("../../model/entities/Topup");
const Payment_1 = require("../../model/entities/Payment");
const Merchant_1 = require("../../model/entities/Merchant");
const Card_1 = require("../../model/entities/Card");
class HeadersContextFiller {
    static fill(user, headers) {
        return __awaiter(this, void 0, void 0, function* () {
            let context = {};
            let accountId = headers["x-resource-account"];
            let topupId = headers["x-resource-topup"];
            let paymentId = headers["x-resource-payment"];
            let merchantId = headers["x-resource-merchant"];
            let cardId = headers["x-resource-card"];
            if (typeof accountId != "undefined") {
                let account = yield Account_1.Account.findOne(accountId);
                if (!account)
                    throw new UnresolvableResourceAccessError_1.UnresolvableResourceAccessError("account", accountId);
                // if((await account.owner).id != user.id) throw new UnauthorizedResourceAccessAttemptError("account", accountId);
                context.account = account;
            }
            if (typeof topupId != "undefined") {
                let topup = yield Topup_1.Topup.findOne(topupId);
                if (!topup)
                    throw new UnresolvableResourceAccessError_1.UnresolvableResourceAccessError("topup", topupId);
                // if((await topup.user).id != user.id) throw new UnauthorizedResourceAccessAttemptError("topup", topupId);
                context.topup = topup;
            }
            if (typeof paymentId != "undefined") {
                let payment = yield Payment_1.Payment.findOne(paymentId);
                if (!payment)
                    throw new UnresolvableResourceAccessError_1.UnresolvableResourceAccessError("payment", paymentId);
                context.payment = payment;
            }
            if (typeof merchantId != "undefined") {
                let merchant = yield Merchant_1.Merchant.findOne(merchantId);
                if (!merchant)
                    throw new UnresolvableResourceAccessError_1.UnresolvableResourceAccessError("merchant", merchantId);
                context.merchant = merchant;
            }
            if (typeof cardId != "undefined") {
                let card = yield Card_1.Card.findOne(cardId);
                if (!card)
                    throw new UnresolvableResourceAccessError_1.UnresolvableResourceAccessError("card", cardId);
                context.card = card;
            }
            return context;
        });
    }
}
exports.HeadersContextFiller = HeadersContextFiller;
//# sourceMappingURL=HeadersContextFiller.js.map