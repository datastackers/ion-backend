"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const vet_1 = require("@zuu/vet");
const Network_1 = require("../network/Network");
var ServiceVersion;
(function (ServiceVersion) {
    ServiceVersion["NOW"] = "-";
})(ServiceVersion = exports.ServiceVersion || (exports.ServiceVersion = {}));
;
class Services {
    static urlFor(service, version) {
        let url = [this.config.app];
        if (service != this.DEFAULT) {
            let descriptor = this.config.services[service];
            if (typeof descriptor == 'string') {
                url.unshift(descriptor);
            }
            else {
                url.unshift(descriptor.id);
                if (descriptor.version && descriptor.version != ServiceVersion.NOW && !version) {
                    url.unshift(descriptor.version);
                }
            }
        }
        if (version && version != ServiceVersion.NOW) {
            url.unshift(version);
        }
        return "https://" + url.join("-dot-") + ".appspot.com/";
    }
    static sockets() {
        return "wss://" + this.config.sockets + "." + this.config.app + ".appspot.com/";
    }
    static build(service, options) {
        if (!this.cache[service] || (!!options && options.force)) {
            let url = this.urlFor(service, (options || {}).version);
            let network = new Network_1.Network(url, this.sockets());
            this.cache[service] = network;
        }
        return this.cache[service];
    }
}
Services.DEFAULT = "default";
Services.USERS = "users";
Services.cache = {};
__decorate([
    vet_1.Configuration("service"),
    __metadata("design:type", Object)
], Services, "config", void 0);
exports.Services = Services;
//# sourceMappingURL=Services.js.map