"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class UnknownServiceError extends Error {
    constructor(service) {
        super("Unknown service <" + service + ">!");
    }
}
exports.UnknownServiceError = UnknownServiceError;
//# sourceMappingURL=UnkownServiceError.js.map