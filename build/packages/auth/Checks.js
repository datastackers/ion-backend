"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const HeadersContextFiller_1 = require("../context/HeadersContextFiller");
const mink_1 = require("@zuu/mink");
const AuthContextChecker_1 = require("../context/AuthContextChecker");
function contextFiller(user, headers) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield HeadersContextFiller_1.HeadersContextFiller.fill(user, headers);
    });
}
exports.contextFiller = contextFiller;
;
function subscriptionCurrentUserChecker(connectionParams, webSocket, context) {
    return __awaiter(this, void 0, void 0, function* () {
        let token = connectionParams['x-access-token'];
        let user = yield AuthContextChecker_1.AuthContextChecker.check(token);
        if (!user)
            throw new mink_1.UnauthorizedError();
        return Object.assign({ user }, (yield HeadersContextFiller_1.HeadersContextFiller.fill(user, connectionParams)));
    });
}
exports.subscriptionCurrentUserChecker = subscriptionCurrentUserChecker;
;
function currentUserChecker(action) {
    return __awaiter(this, void 0, void 0, function* () {
        let token = action.request.headers["x-access-token"];
        return yield AuthContextChecker_1.AuthContextChecker.check(token);
    });
}
exports.currentUserChecker = currentUserChecker;
;
//# sourceMappingURL=Checks.js.map