"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const crypto_1 = require("crypto");
const vet_1 = require("@zuu/vet");
var HashingAlgorithm;
(function (HashingAlgorithm) {
    HashingAlgorithm[HashingAlgorithm["SHA512"] = 0] = "SHA512";
})(HashingAlgorithm = exports.HashingAlgorithm || (exports.HashingAlgorithm = {}));
;
class HashedPassword {
    constructor(hash, salt, algo) {
        this.hash = hash;
        this.salt = salt;
        this.algo = algo;
    }
    ;
}
exports.HashedPassword = HashedPassword;
;
let Hash = class Hash {
    update(password, salt, algo) {
        let hasher = crypto_1.createHmac('sha512', salt);
        hasher.update(password);
        let hash = hasher.digest('hex');
        return new HashedPassword(hash, salt, algo);
    }
    same(input, check) {
        let hash = this.update(input, check.salt, check.algo).hash;
        return hash == check.hash;
    }
    compile(password) {
        return 'sftw-' + password.salt + '-' + password.hash;
    }
    decompile(password) {
        let components = password.split("-");
        if (components.length != 3)
            return null;
        return new HashedPassword(components[2], components[1], HashingAlgorithm.SHA512);
    }
};
Hash = __decorate([
    vet_1.Singleton
], Hash);
exports.Hash = Hash;
//# sourceMappingURL=Hash.js.map