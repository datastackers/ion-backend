"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Class used as a context container
 */
class Context {
    constructor(token = null) {
        this.headers = {};
        this.authorize(token);
    }
    /**
     * Compile and export the generated stores into a headers collection
     * @returns The compiled headers collection
     */
    export() {
        return this.headers;
    }
    /**
     * Sets a value inside the headers store for later usage.
     * @param key The key in the headers store.
     * @param value The value that will be set.
     * @returns The context (for chaining)
     */
    header(key, value) {
        if (!value)
            delete this.headers[key];
        else
            this.headers[key] = value;
        return this;
    }
    /**
     * Authorize the current context agains a set of credentials.
     * @param credentials Set of credentials used in the authorization process
     * @returns The context (for chaining)
     */
    authorize(token) {
        if (!token)
            return this;
        if (!!token)
            this.token = token;
        this.header(Context.ACCESS_TOKEN_KEY, this.token);
        return this;
    }
    /**
     * Checks is the context is authorized.
     *
     * @returns Authorization state
     */
    isAuthorized() {
        return !!this.token;
    }
    /**
     * Sets a value inside the resource store for later usage.
     * @param name The name of the resource
     * @param value The value of the resource
     * @returns The context (for chaining)
     */
    resource(name, value) {
        if (!!name)
            this.header(`${Context.RESOURCE_KEY_PREFIX}${name}`, value);
        return this;
    }
}
Context.ACCESS_TOKEN_KEY = 'x-access-token';
Context.RESOURCE_KEY_PREFIX = 'x-resource-';
exports.Context = Context;
//# sourceMappingURL=Context.js.map