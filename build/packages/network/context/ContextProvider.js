"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Context_1 = require("./Context");
/**
 * Provides the context.
 */
class ContextProvider {
    constructor() { }
    ;
    /**
     * This method tries to export an optimal context using the 'Optimistic Provider' pattern.
     * @returns A context
     */
    exportOptimalContext() {
        let context = new Context_1.Context;
        // let optimisticCredentials = this.credentialsProvider.export(true);
        // if(!!optimisticCredentials) {
        //     context.authorize(optimisticCredentials);
        // }
        return context;
    }
}
exports.ContextProvider = ContextProvider;
//# sourceMappingURL=ContextProvider.js.map