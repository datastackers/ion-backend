"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ContextProvider_1 = require("./context/ContextProvider");
const HttpLayer_1 = require("./layers/HttpLayer");
const GraphLayer_1 = require("./layers/GraphLayer");
const Executor_1 = require("./execution/Executor");
const _ = require("lodash");
class Network {
    constructor(url, socketsUrl) {
        this.contextProvider = new ContextProvider_1.ContextProvider();
        this.http = new HttpLayer_1.HttpLayer(url);
        this.graph = new GraphLayer_1.GraphLayer(url, socketsUrl, 'api/graph', 'api/graph/subscriptions');
        this.graph.init(this.contextProvider.exportOptimalContext());
    }
    ;
    updateContext() {
        this.graph.swapContext(this.contextProvider.exportOptimalContext());
    }
    ;
    isAuthorized() {
        return this.contextProvider.exportOptimalContext().isAuthorized();
    }
    ;
    copy(obj) {
        return _.cloneDeep(obj);
    }
    ;
    scope(executorFunction, contextFunction = (x) => x) {
        return __awaiter(this, void 0, void 0, function* () {
            let ctx = this.contextProvider.exportOptimalContext();
            ctx = yield contextFunction(ctx);
            let executor = new Executor_1.Executor(ctx, this.http, this.graph);
            let result;
            result = yield executorFunction(executor);
            executor = null;
            return this.copy(result);
        });
    }
    ;
    subscription(executorFunction, contextFunction = (x) => x) {
        return __awaiter(this, void 0, void 0, function* () {
            let ctx = this.contextProvider.exportOptimalContext();
            ctx = yield contextFunction(ctx);
            let executor = new Executor_1.Executor(ctx, this.http, this.graph);
            let result;
            result = yield executorFunction(executor);
            executor = null;
            return this.copy(result);
        });
    }
    ;
}
exports.Network = Network;
;
//# sourceMappingURL=Network.js.map