"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Class responsible for dispatching requests to the network layers.
 */
class Executor {
    constructor(context, http, graph) {
        this.context = context;
        this.http = http;
        this.graph = graph;
    }
    ;
    /**
     * Makes a classic GET request
     * @param method API method to call
     * @returns Promise to the result
     */
    get(method) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.http.get({ method, headers: this.context.export() });
        });
    }
    ;
    /**
     * Makes a classic POST request
     * @param method API method to call
     * @param data Data for the body
     * @returns Promise to the result
     */
    post(method, data = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.http.post({ method, data, headers: this.context.export() });
        });
    }
    ;
    /**
     * Makes a classic REST request. The network layer will decide if it will be a GET or a POST.
     * @param method API method to call
     * @returns Promise to the result
     */
    call(method, data = null) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.http.call({ method, data, headers: this.context.export() });
        });
    }
    ;
    /**
     * Makes a query to the Graph
     * @param query The query
     * @param options Any other options
     * @returns Promise to the result
     */
    query(query, options) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.graph.client().query(Object.assign({ query, context: { headers: this.context.export() } }, options));
        });
    }
    ;
    /**
     * Makes a mutation to the Graph
     * @param query The query
     * @param options Any other options
     * @returns Promise to the result
     */
    mutate(mutation, options) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.graph.client().mutate(Object.assign({ mutation, context: { headers: this.context.export() } }, options));
        });
    }
    ;
    /**
     * Makes a subscription to the Graph
     * @param query The query
     * @param options Any other options
     * @returns Promise to the result
     */
    subscribe(query, options) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.graph.client().subscribe(Object.assign({ query }, options));
        });
    }
    ;
}
exports.Executor = Executor;
;
//# sourceMappingURL=Executor.js.map