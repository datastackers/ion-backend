"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Credentials_1 = require("./Credentials");
class LocalStorageCredentialsProvider {
    constructor(key) {
        this.key = key;
        this.cached = null;
    }
    ;
    export(force = true) {
        if (!this.cached || force) {
            let compiledTokens = localStorage.getItem(this.key);
            if (!compiledTokens)
                this.cached = null;
            else
                this.cached = new Credentials_1.Credentials(compiledTokens);
        }
        return this.cached;
    }
    save(credentials) {
        this.cached = credentials;
        localStorage.setItem(this.key, this.cached.compile());
        return true;
    }
    revoke() {
        this.cached = null;
        localStorage.removeItem(this.key);
        return true;
    }
}
exports.LocalStorageCredentialsProvider = LocalStorageCredentialsProvider;
//# sourceMappingURL=LocalStorageCredentialsProvider.js.map