"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_client_1 = require("apollo-client");
const apollo_link_1 = require("apollo-link");
const apollo_link_http_1 = require("apollo-link-http");
const apollo_link_ws_1 = require("apollo-link-ws");
const apollo_utilities_1 = require("apollo-utilities");
const apollo_cache_inmemory_1 = require("apollo-cache-inmemory");
const ws = require("ws");
const fetch = require("node-fetch");
/**
 * Network layer responsible for graphql queries interaction
 */
class GraphLayer {
    constructor(uri, socketsUri, graphUri, subscriptionsUri) {
        this.uri = uri;
        this.socketsUri = socketsUri;
        this.graphUri = graphUri;
        this.subscriptionsUri = subscriptionsUri;
    }
    ;
    provideConnectionParameters() {
        return this.context.export();
    }
    ;
    /**
     * Changes the current context.
     * @param context The alternative context
     */
    swapContext(context) {
        this.context = context;
    }
    ;
    /**
     * Initialize the layer using a start context (providing the auth headers for the ws connection)
     * @param context The initialization context
     */
    init(context) {
        this.swapContext(context);
        this.httpLink = new apollo_link_http_1.HttpLink({ uri: this.uri + this.graphUri, fetch });
        this.wsLink = new apollo_link_ws_1.WebSocketLink({
            uri: this.socketsUri + this.subscriptionsUri, options: {
                reconnect: false,
                connectionParams: this.provideConnectionParameters.bind(this)
            }, webSocketImpl: ws
        });
        this.splitedLink = apollo_link_1.split(({ query }) => {
            const { kind, operation } = apollo_utilities_1.getMainDefinition(query);
            return kind === 'OperationDefinition' && operation === 'subscription';
        }, this.wsLink, this.httpLink);
        this._client = new apollo_client_1.default({
            link: this.splitedLink,
            cache: new apollo_cache_inmemory_1.InMemoryCache(),
            defaultOptions: {
                query: {
                    fetchPolicy: 'network-only',
                    errorPolicy: 'all',
                },
            }
        });
    }
    ;
    /**
     * Export the final client.
     * @returns GraphQL client
     */
    client() {
        return this._client;
    }
}
exports.GraphLayer = GraphLayer;
//# sourceMappingURL=GraphLayer.js.map