"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TokenScope;
(function (TokenScope) {
    TokenScope[TokenScope["AUTH"] = 0] = "AUTH";
    TokenScope[TokenScope["REFRESH"] = 1] = "REFRESH";
})(TokenScope = exports.TokenScope || (exports.TokenScope = {}));
;
//# sourceMappingURL=TokenScope.js.map