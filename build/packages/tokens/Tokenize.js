"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var Tokenize_1;
const vet_1 = require("@zuu/vet");
const Token_1 = require("../../model/entities/Token");
const jwt = require("jsonwebtoken");
let Tokenize = Tokenize_1 = class Tokenize {
    constructor() {
        Tokenize_1._instance = this;
    }
    static instance() {
        if (!this._instance)
            return new Tokenize_1();
        return this._instance;
    }
    create(target, scope, expires = true) {
        return __awaiter(this, void 0, void 0, function* () {
            let token = new Token_1.Token();
            token.scope = scope;
            token.target = target;
            yield token.save();
            let payload = {
                target, scope,
                id: token.id
            };
            let chars;
            if (!expires)
                chars = yield jwt.sign(payload, this.securityConfig.token.secret, { expiresIn: this.securityConfig.token.expires });
            else
                chars = yield yield jwt.sign(payload, this.securityConfig.token.secret);
            token.chars = chars;
            yield token.save();
            return token;
        });
    }
    find(chars, scope) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let decompile = yield jwt.verify(chars, this.securityConfig.token.secret);
            }
            catch (ex) {
                return null;
            }
            let token = yield Token_1.Token.findOne({ chars, scope });
            if (!token)
                return null;
            return token;
        });
    }
};
__decorate([
    vet_1.Configuration("security"),
    __metadata("design:type", Object)
], Tokenize.prototype, "securityConfig", void 0);
Tokenize = Tokenize_1 = __decorate([
    vet_1.Singleton,
    __metadata("design:paramtypes", [])
], Tokenize);
exports.Tokenize = Tokenize;
//# sourceMappingURL=Tokenize.js.map