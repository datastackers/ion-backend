"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const vet_1 = require("@zuu/vet");
const braintree = require("braintree");
class Braintree {
    constructor() {
        this.gateway = braintree.connect({
            environment: braintree.Environment.Sandbox,
            merchantId: Braintree.config.id,
            publicKey: Braintree.config.public,
            privateKey: Braintree.config.private
        });
    }
    createCustomer(data) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield (new Promise((resolve, reject) => {
                this.gateway.customer.create(data, function (err, result) {
                    if (err)
                        return reject(err);
                    resolve(result.customer.id);
                });
            }));
        });
    }
    clientToken(id) {
        return __awaiter(this, void 0, void 0, function* () {
            let options = { customerId: id };
            if (!id)
                delete options.customerId;
            return yield (new Promise((resolve, reject) => {
                this.gateway.clientToken.generate(options, function (err, response) {
                    if (!!err)
                        return reject(err);
                    console.log(response);
                    resolve(response.clientToken);
                });
            }));
        });
    }
    sale(ammount, currency, nonce) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield (new Promise((resolve, reject) => {
                this.gateway.transaction.sale({
                    amount: ammount,
                    // currency: currency.toUpperCase(),
                    paymentMethodNonce: nonce,
                    options: {
                        submitForSettlement: true
                    }
                }, function (err, result) {
                    if (!!err)
                        return reject(err);
                    resolve(true);
                });
            }));
        });
    }
}
__decorate([
    vet_1.Configuration("braintree"),
    __metadata("design:type", Object)
], Braintree, "config", void 0);
exports.Braintree = Braintree;
//# sourceMappingURL=Braintree.js.map