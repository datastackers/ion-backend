"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const bootstrap_1 = require("@zuu/bootstrap");
const vet_1 = require("@zuu/vet");
const ResponseFormatter_1 = require("./interceptors/ResponseFormatter");
const ListeningEventListener_1 = require("./listeners/ListeningEventListener");
const ResponseTime_1 = require("./middlewares/ResponseTime");
const Timer_1 = require("./packages/timer/Timer");
const Checks_1 = require("./packages/auth/Checks");
const AliveController_1 = require("./controllers/AliveController");
const AccountResolver_1 = require("./resolvers/AccountResolver");
const TopupResolver_1 = require("./resolvers/TopupResolver");
const VisualisationResolver_1 = require("./resolvers/VisualisationResolver");
vet_1.Debugger.deafults();
let tag = vet_1.Debugger.tag("service-accounts");
let options = {
    server: {
        port: parseInt(process.env["PORT"]) || 4100,
        modules: []
    },
    currentUserChecker: Checks_1.currentUserChecker,
    graph: {
        contextFiller: Checks_1.contextFiller, subscriptionCurrentUserChecker: Checks_1.subscriptionCurrentUserChecker
    },
    listeners: [
        new ListeningEventListener_1.ListeningEventListener
    ],
    middlewares: [
        ResponseTime_1.ResponseTime
    ],
    interceptors: [
        ResponseFormatter_1.ResponseFormatter
    ],
    controllers: [
        AliveController_1.AliveController,
        bootstrap_1.AuthedGraphQLController
    ],
    resolvers: [
        AccountResolver_1.AccountResolver,
        TopupResolver_1.TopupResolver,
        VisualisationResolver_1.VisualisationResolver
    ],
    cors: true
};
let timer = new Timer_1.Timer().reset();
vet_1.Runtime.scoped(null, (_) => __awaiter(this, void 0, void 0, function* () {
    vet_1.Debugger.log(tag `Initialization began!`);
    let { app } = yield bootstrap_1.Bootstrap.scope(options).run();
    return (typeof app != "undefined" && app != null);
}))
    .then((result) => __awaiter(this, void 0, void 0, function* () {
    vet_1.Debugger.log(tag `Initialization succeeded! Took ${timer.stop().diff()}ms!`);
}))
    .catch(vet_1.Debugger.error);
//# sourceMappingURL=accounts.js.map