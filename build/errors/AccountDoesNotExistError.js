"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mink_1 = require("@zuu/mink");
class AccountDoesNotExistError extends mink_1.BadRequestError {
    constructor(identity) {
        super("An account identified by \"" + identity + "\" does not exist.");
        this.name = "AccountDoesNotExistError";
    }
}
exports.AccountDoesNotExistError = AccountDoesNotExistError;
//# sourceMappingURL=AccountDoesNotExistError.js.map