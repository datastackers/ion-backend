"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mink_1 = require("@zuu/mink");
class ClientDoesNotExistError extends mink_1.BadRequestError {
    constructor(identity) {
        super("A client identified by \"" + identity + "\" does not exist.");
        this.name = "ClientDoesNotExistError";
    }
}
exports.ClientDoesNotExistError = ClientDoesNotExistError;
//# sourceMappingURL=ClientDoesNotExistError.js.map