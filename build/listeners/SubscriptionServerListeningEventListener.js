"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bootstrap_1 = require("@zuu/bootstrap");
const vet_1 = require("@zuu/vet");
let tag = vet_1.Debugger.tag("subscription-server-listening-event-listener");
class SubscriptionServerListeningEventListener extends vet_1.AbstractEventListener {
    constructor() {
        super(bootstrap_1.BootstrapEvents.SUBSCRIPTION_SERVER_LISTENING);
    }
    handle(event, prevent) {
        vet_1.Debugger.log(tag `Subscriptions are available under wss://*:*${bootstrap_1.GQLHelper.subscriptionsPath}`);
    }
}
exports.SubscriptionServerListeningEventListener = SubscriptionServerListeningEventListener;
//# sourceMappingURL=SubscriptionServerListeningEventListener.js.map